# SOFEA: A Non-iterative and Robust Optical Flow Estimation Algorithm for Dynamic Vision Sensors

## Overview
This repository contains a MATLAB implementation of the **Single-shot Optical Flow Estimation Algorithm (SOFEA)**, as presented in the CVPR 2020 workshop paper ["SOFEA: A Non-iterative and Robust Optical Flow Estimation Algorithm for Dynamic Vision"](https://openaccess.thecvf.com/content_CVPRW_2020/html/w6/Low_SOFEA_A_Non-Iterative_and_Robust_Optical_Flow_Estimation_Algorithm_for_CVPRW_2020_paper.html).

## Contents
- `sequences`: Folder containing all three sequences used for the experiments of this work (`stripes`, `rotating_bar` and `slider_hdr_far` sequences)
- `sofea.m`: MATLAB script implementing SOFEA

## Event Sequence Format
Each MAT-file (version 7.3) contains a `TD` structure with the following fields:

- `TD.x`: A row vector of event x-axis pixel locations (1-based indexing)
- `TD.y`: A row vector of event y-axis pixel locations (1-based indexing)
- `TD.ts`: A row vector of event timestamps in microseconds
- `TD.p`: A row vector of event polarities (0: negative, 1: positive)

This event sequence encoding follows that of the AER vision data introduced [here](https://github.com/gorchard/Matlab_AER_vision_functions).

## Citation
If you find this work useful for your research, please consider citing it with the following BibTeX entry:
```
@InProceedings{Low_2020_CVPR_Workshops,
    author = {Low, Weng Fei and Gao, Zhi and Xiang, Cheng and Ramesh, Bharath},
    title = {SOFEA: A Non-Iterative and Robust Optical Flow Estimation Algorithm for Dynamic Vision Sensors},
    booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR) Workshops},
    month = {June},
    year = {2020}
}
```
