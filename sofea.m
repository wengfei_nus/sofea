clear;

SEQUENCE = 'rotating_bar';   % sequence name [stripes, rotating_bar, slider_hdr_far]

scriptPath = matlab.desktop.editor.getActiveFilename;
folderPath = scriptPath(1 : end-8);
sequenceMatPath = fullfile(folderPath, 'sequences', strcat(SEQUENCE, '.mat'));

% load saved sequence workspace variable 'TD'
load(sequenceMatPath);

%%
% event replay simulation parameters
SimParams.FIRST_EVENT_IDX = 1;          % index of first event to be replayed
SimParams.LAST_EVENT_IDX = 180000;      % index of last event to be replayed                                         
SimParams.OPT_FLOW_EST_STEPS = 170000;  % number of events, counting backwards
                                        % from 'SimParams.LAST_EVENT_IDX',
                                        % with optical flow estimates

% SOFEA parameters
OptFlowEstParams.L = 7;                 % px (only odd numbers)
OptFlowEstParams.T_RF = 40000;          % us
OptFlowEstParams.N_NB = 16;                                  
OptFlowEstParams.E = 11000;             % us
OptFlowEstParams.N_SP = 15;                                  

% derived parameters
screenSize = get(groot, 'ScreenSize');	% get [left bottom width height]
                                        % screen size information (in 
                                        % pixels) about the current screen
                                        
Neighbours.length = OptFlowEstParams.L;
Neighbours.halfLength = floor((Neighbours.length - 1) / 2);
Neighbours.count = Neighbours.length ^ 2;
Neighbours.centerIdx = Neighbours.halfLength + 1;	% x/y index of EOI
[ Neighbours.relativeX, Neighbours.relativeY ] ...
    = meshgrid([ 1 : OptFlowEstParams.L ], [ 1 : OptFlowEstParams.L ]);
Neighbours.relativeP = [ Neighbours.relativeX(:), Neighbours.relativeY(:) ];	% column major order
Neighbours.deltaX = Neighbours.relativeX - Neighbours.centerIdx;
Neighbours.deltaY = Neighbours.relativeY - Neighbours.centerIdx;
Neighbours.deltaP = [ Neighbours.deltaX(:), Neighbours.deltaY(:) ];
            
FrameRes.x = max(TD.x);
FrameRes.y = max(TD.y);
FrameRes.size = [ FrameRes.y, FrameRes.x ];

% initialisations
Events.lastP = zeros(FrameRes.size);
Events.lastTs = zeros(FrameRes.size);                                                       

OptFlowEval.velocities = zeros(SimParams.OPT_FLOW_EST_STEPS, 2);
OptFlowEval.x = zeros(SimParams.OPT_FLOW_EST_STEPS, 1);
OptFlowEval.y = zeros(SimParams.OPT_FLOW_EST_STEPS, 1);
OptFlowEval.p = zeros(SimParams.OPT_FLOW_EST_STEPS, 1);
OptFlowEval.ts = zeros(SimParams.OPT_FLOW_EST_STEPS, 1);
OptFlowEval.eventIdx = 1;

% event-by-event process loop
for eventIdx = SimParams.FIRST_EVENT_IDX : SimParams.LAST_EVENT_IDX
    Eoi.x = TD.x(eventIdx);
    Eoi.y = TD.y(eventIdx);
    Eoi.p = TD.p(eventIdx);
    Eoi.ts = TD.ts(eventIdx);    
           
    % constant refractory period filtering
    if (Eoi.ts - Events.lastTs(Eoi.y, Eoi.x) < OptFlowEstParams.T_RF)
        continue;
    end
    
    % record all 'Eoi.ts' & 'Eoi.p' except for refractory filtered ones
    Events.lastTs(Eoi.y, Eoi.x) = Eoi.ts;
    Events.lastP(Eoi.y, Eoi.x) = Eoi.p;
    
    % only estimate optical flow from 'SimParams.LAST_EVENT_IDX' -
    % 'SimParams.OPT_FLOW_EST_STEPS + 1' onwards
    if (eventIdx <= SimParams.LAST_EVENT_IDX - SimParams.OPT_FLOW_EST_STEPS)
        continue;
    end
 
    % estimate optical flow of the EOI
    
    % greedy selection of 'OptFlowEstParams.N_NB' associated
    % spatial neighbours of the EOI    
    
    % only process EOI with 'OptFlowEstParams.L x OptFlowEstParams.L' neighbours
    NbPosRange.xl = Eoi.x - Neighbours.halfLength;
    NbPosRange.xh = Eoi.x + Neighbours.halfLength;
    NbPosRange.yl = Eoi.y - Neighbours.halfLength;
    NbPosRange.yh = Eoi.y + Neighbours.halfLength;    
    if (NbPosRange.xl < 1 || NbPosRange.xh > FrameRes.x ...
        || NbPosRange.yl < 1 || NbPosRange.yh > FrameRes.y)        
        continue;
    end
        
    % extract spatial neighbours of the EOI
    Neighbours.lastTs = Events.lastTs(NbPosRange.yl : NbPosRange.yh, ...
                                      NbPosRange.xl : NbPosRange.xh);
    Neighbours.lastP = Events.lastP(NbPosRange.yl : NbPosRange.yh, ...
                                    NbPosRange.xl : NbPosRange.xh);
    Neighbours.isSamePolarity = (Eoi.p == Neighbours.lastP);

    % sort neighbouring events based on their timestamps
    Neighbours.lastTs(Neighbours.centerIdx, Neighbours.centerIdx) ...           % done to ensure the EOI
        = Neighbours.lastTs(Neighbours.centerIdx, Neighbours.centerIdx) + 1;    % has the largest timestamp
    [ ~, Neighbours.tsSortLinIdx ] = sort(Neighbours.lastTs(:), 'descend');
    Neighbours.lastTs(Neighbours.centerIdx, Neighbours.centerIdx) ...
        = Neighbours.lastTs(Neighbours.centerIdx, Neighbours.centerIdx) - 1;
    
    % select associated neighbouring events
    Neighbours.associatedCount = 0;
    Neighbours.isAssociated = false(OptFlowEstParams.L);
    Neighbours.isCollinear = false(OptFlowEstParams.L); % indicates the last selected 
                                                        % neighbouring events that cause
                                                        % spatial collinearity of
                                                        % 'OptFlowEst.deltaP'
    Neighbours.inCoverage = false(OptFlowEstParams.L);  % indicates the neighbouring events
                                                        % (locations) that are 8-neighbours
                                                        % of associated and collinear events
    
	% initialise 'Neighbours.inCoverage' with 8-neighbours of the EOI
    NbPosRange.xl = max(Neighbours.centerIdx - 1, 1);
    NbPosRange.xh = min(Neighbours.centerIdx + 1, OptFlowEstParams.L);
    NbPosRange.yl = max(Neighbours.centerIdx - 1, 1);
    NbPosRange.yh = min(Neighbours.centerIdx + 1, OptFlowEstParams.L);
    
    isDirectNb = false(Neighbours.length);
    isDirectNb(NbPosRange.yl : NbPosRange.yh, NbPosRange.xl : NbPosRange.xh) = true;
    isDirectNb(Neighbours.centerIdx, Neighbours.centerIdx) = false;     % exclude EOI
    
    Neighbours.inCoverage ...	
        = Neighbours.inCoverage | isDirectNb;
    
    % collinear check is required only if
    % 'OptFlowEstParams.N_NB < OptFlowEstParams.L'
    CollinearCheck.isCollinear ...
        = (OptFlowEstParams.N_NB < OptFlowEstParams.L);
    CollinearCheck.collinearTypeCount = 0;	% number of spatially collinear 
                                            % arrangement types present in 
                                            % the selected associated
                                            % neighbouring events of the EOI
    CollinearCheck.collinearTypeExists = false(1, 4);	% existence of collinear 
                                                        % arrangement types    
    
    OptFlowEst.deltaP = zeros(OptFlowEstParams.N_NB, 2);
    OptFlowEst.deltaTs = zeros(OptFlowEstParams.N_NB, 1);
    
    sortIdx = 2;    % ignore EOI (as it has the largest timestamp)
    while (Neighbours.associatedCount < OptFlowEstParams.N_NB ...
           && sortIdx <= Neighbours.count)
        % linear index, which indicates the position, of the currently
        % considered neighbouring event
        neighbourLinIdx = Neighbours.tsSortLinIdx(sortIdx);
        
        if (~Neighbours.isSamePolarity(neighbourLinIdx) ...
            || ~Neighbours.inCoverage(neighbourLinIdx) ...
            || Neighbours.isAssociated(neighbourLinIdx) ...
            || Neighbours.isCollinear(neighbourLinIdx))
            sortIdx = sortIdx + 1;
            continue; 
        end
        
        % next associated neighbour found, if not limited by spatial
        % collinearity
        NewEvent.relativeP = Neighbours.relativeP(neighbourLinIdx, :);
        NewEvent.deltaP = Neighbours.deltaP(neighbourLinIdx, :);
        NewEvent.deltaTs = Neighbours.lastTs(neighbourLinIdx) - Eoi.ts;
        
        % update coverage
        NbPosRange.xl = max(NewEvent.relativeP(1) - 1, 1);
        NbPosRange.xh = min(NewEvent.relativeP(1) + 1, OptFlowEstParams.L);
        NbPosRange.yl = max(NewEvent.relativeP(2) - 1, 1);
        NbPosRange.yh = min(NewEvent.relativeP(2) + 1, OptFlowEstParams.L);

        isDirectNb = false(Neighbours.length);
        isDirectNb(NbPosRange.yl : NbPosRange.yh, NbPosRange.xl : NbPosRange.xh) = true;
        isDirectNb(NewEvent.relativeP(2), NewEvent.relativeP(1)) = false;	% exclude new event    

        Neighbours.inCoverage ...	
            = Neighbours.inCoverage | isDirectNb;
        
        % collinearity check
        if (CollinearCheck.isCollinear)        
            % check collinear arrangement type of newly selected event
            collinearType = 0;          % indicates a non-collinear 
                                        % arrangement by default
            if (NewEvent.deltaP(1) == 0)   
                collinearType = 1;
            elseif (NewEvent.deltaP(2) == 0)
                collinearType = 2;
            elseif (NewEvent.deltaP(1) == NewEvent.deltaP(2))
                collinearType = 3;
            elseif (NewEvent.deltaP(1) == -NewEvent.deltaP(2))
                collinearType = 4;
            end
            
            % update collinearity check variables
            if (collinearType == 0)
                CollinearCheck.isCollinear = false;
            elseif (~CollinearCheck.collinearTypeExists(collinearType)) % && collinearType ~= 0
                CollinearCheck.collinearTypeExists(collinearType) = true;
                if (CollinearCheck.collinearTypeCount == 0)
                    CollinearCheck.collinearTypeCount = 1;
                else	% if (CollinearCheck.collinearTypeCount == 1)
                    CollinearCheck.collinearTypeCount = 2;
                    CollinearCheck.isCollinear = false;
                end
            end
            
            % extend greedy selection if collinearity persists until
            % 'Neighbours.associatedCount == OptFlowEstParams.L - 1'
            if (CollinearCheck.isCollinear ...
                && Neighbours.associatedCount ...
                   == OptFlowEstParams.N_NB - 1)
                Neighbours.isCollinear(neighbourLinIdx) = true;
                sortIdx = 2;
                continue;
            end
        end
        
        Neighbours.associatedCount = Neighbours.associatedCount + 1;
        Neighbours.isAssociated(neighbourLinIdx) = true;
        OptFlowEst.deltaTs(Neighbours.associatedCount) = NewEvent.deltaTs;
        OptFlowEst.deltaP(Neighbours.associatedCount, :) = NewEvent.deltaP;          
        sortIdx = 2;
    end
    
    % filtering due to insufficient associated neighbouring events selected
    % for non-iterative plane fitting
    if (Neighbours.associatedCount < OptFlowEstParams.N_NB)
        continue;
    end
    
    % estimate optical flow based on plane fitting/concept of directional 
    % derivative, fits only the gradient of the plane. Only a 2x2 matrix
    % inversion required.
    % Equation - delta ts = dot(grad ts, delta p)
    OptFlowEst.gradient = OptFlowEst.deltaP \ OptFlowEst.deltaTs;
    OptFlowEst.gradientNorm = norm(OptFlowEst.gradient);

    % goodness-of-fit noise rejection via plane support evaluation 
    % (valid events have the same polarity as the EOI)
    Neighbours.samePolDeltaTs ...
        = Neighbours.lastTs(Neighbours.isSamePolarity) - Eoi.ts;        
    OptFlowEst.samePolEstDeltaTs ...
        = Neighbours.deltaP(Neighbours.isSamePolarity(:), :) ...
          * OptFlowEst.gradient;
    OptFlowEst.samePolDeltaTsResidual ...
        = abs(Neighbours.samePolDeltaTs - OptFlowEst.samePolEstDeltaTs);
    OptFlowEst.planeSupportCount ...    % excluding the EOI
        = sum(OptFlowEst.samePolDeltaTsResidual ...
              < OptFlowEstParams.E) - 1;  

    % 'OptFlowEst.gradientNorm == 0' happens when 'OptFlowEst.deltaTs'
    %  is a zero vector
    if (OptFlowEst.gradientNorm == 0 ...
        || OptFlowEst.planeSupportCount < OptFlowEstParams.N_SP)
        continue;
    end
    
    % this calculation prevents the issue of infinite velocity component 
    OptFlowEst.normalisedGrad ...
        = OptFlowEst.gradient / OptFlowEst.gradientNorm;
    OptFlowEst.velocity ...
        = (1 / OptFlowEst.gradientNorm) * OptFlowEst.normalisedGrad;

    % record critical information for optical flow evaluation
    OptFlowEval.velocities(OptFlowEval.eventIdx, :) = OptFlowEst.velocity;
    OptFlowEval.x(OptFlowEval.eventIdx) = Eoi.x;
    OptFlowEval.y(OptFlowEval.eventIdx) = Eoi.y;
    OptFlowEval.p(OptFlowEval.eventIdx) = Eoi.p;
    OptFlowEval.ts(OptFlowEval.eventIdx) = Eoi.ts;
    OptFlowEval.eventIdx = OptFlowEval.eventIdx + 1;
    
    %{
    % visualise the local spatial neighbourhood about the EOI
    Neighbours.x = Neighbours.deltaX + Eoi.x;
    Neighbours.y = Neighbours.deltaY + Eoi.y;
    
    figure('OuterPosition', screenSize);
    scatter3(Neighbours.x(Neighbours.isSamePolarity), Neighbours.y(Neighbours.isSamePolarity), ...
             Neighbours.lastTs(Neighbours.isSamePolarity), 500, 'filled');
    hold on;
    scatter3(OptFlowEst.deltaP(:, 1) + Eoi.x, OptFlowEst.deltaP(:, 2) + Eoi.y, ...
             OptFlowEst.deltaTs + Eoi.ts, 300, 'filled');
    scatter3(Eoi.x, Eoi.y, Eoi.ts, 300, 'filled');
    text(Neighbours.x(Neighbours.tsSortLinIdx), Neighbours.y(Neighbours.tsSortLinIdx), ...
         Neighbours.lastTs(Neighbours.tsSortLinIdx), ...
         mat2cell((1 : numel(Neighbours.tsSortLinIdx))', ones(numel(Neighbours.tsSortLinIdx), 1)), ...
         'Color', 'magenta', 'FontSize', 20);
    surf(Neighbours.x, Neighbours.y, Neighbours.lastTs);
    hold off;

    % wait for keypress to proceed with next event
    while ~waitforbuttonpress
    end
    
    close all;
    %}
end

% trim optical flow vectors/matrices
OptFlowEval.length = OptFlowEval.eventIdx - 1;
OptFlowEval.velocities = OptFlowEval.velocities(1 : OptFlowEval.length, :);
OptFlowEval.x = OptFlowEval.x(1 : OptFlowEval.length);
OptFlowEval.y = OptFlowEval.y(1 : OptFlowEval.length);
OptFlowEval.p = OptFlowEval.p(1 : OptFlowEval.length);
OptFlowEval.ts = OptFlowEval.ts(1 : OptFlowEval.length);
